const createTable = () => {

  const userData = [
    ['Nitish', '22', '09/01/1998', 'nitish19@gmail.com', 'EazyDiner'],
    ['Aman', '23', '10/01/1998', 'aman19@gmail.com', 'Yulu'],
    ['Mayank', '22', '23/11/1998', 'mayank19@gmail.com', 'Gojek'],
    ['Akash', '21', '10/03/1997', 'akash19@gmail.com', 'Amazon'],
    ['Ankit', '22', '09/01/1993', 'ankit19@gmail.com', 'Samsung'],
  ];

  const body = document.getElementsByTagName('body')[0];
  const table = document.createElement('table');
  
  //CREATING H1 ELEMENT 
  const h1 = document.createElement('h1');
  const h1Text = document.createTextNode('EazyDiner');
  h1.append(h1Text);
  
  // CREATING TABLE HEAD 
  const tableHead = document.createElement('thead');
  let tableRow = document.createElement('tr');
  const label = ['Name', 'Age', 'DOB', 'Email', 'Company'];

  for (let j = 0; j < 5; j += 1) {
    const tableCell = document.createElement('th');
    const cellText = document.createTextNode(label[j]);
    tableCell.appendChild(cellText);
    tableRow.appendChild(tableCell);
  }
  tableHead.appendChild(tableRow);
  table.appendChild(tableHead);
  
  // CREATING TABLE BODY 
  const tableBody = document.createElement('tbody');

  for (let i = 0; i < 5; i += 1) {
    let tableRow = document.createElement('tr');
    for (let j = 0; j < 5; j += 1) {
      const tableCell = document.createElement('td');
      const cellText = document.createTextNode(userData[i][j]);
      tableCell.appendChild(cellText);
      tableRow.appendChild(tableCell);
    }
    tableBody.appendChild(tableRow);
  }

  table.appendChild(tableBody);
  body.appendChild(h1);
  body.appendChild(table);
};

const styleTable = () => {

  // STYLES 
  const h1 = document.getElementsByTagName('h1')[0];
  const table = document.getElementsByTagName('table')[0];
  const tds = document.getElementsByTagName('td');
  const ths = document.getElementsByTagName('th');
  const body = document.getElementsByTagName('body')[0];
  
  h1.setAttribute('align', 'center');

  table.setAttribute('border', '5px solid black');
  table.setAttribute('width', '50%');
  table.style.background = 'Tomato';
  table.setAttribute('align', 'center');

  for(let i = 0; i < tds.length; i += 1){
    tds[i].setAttribute('height', '40px');
    tds[i].style.padding = '10px';
    tds[i].style.textAlign = 'center';
  }
  
  for(let i = 0; i < ths.length; i += 1){
    ths[i].setAttribute('height', '40px');
    ths[i].style.background = 'Orange';
  }

  body.style.color = 'white';
  body.style.background = 'grey';
  body.style.margin = '20px 10px 10px 10px';
}

module.exports = { createTable, styleTable } ;
